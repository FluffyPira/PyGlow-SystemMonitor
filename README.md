# Pira's PyGlow System Monitor
### Version 0.1
System monitor using PyGlow and written in Python to use the PiGlow by Pimoroni board to visually indicate CPU load, Memory use, and Hard-drive use.

The script was adapted from the example CPU monitor found [here](https://github.com/benleb/PyGlow/blob/master/examples/cpu.py)

This script requires the [PyGlow library](https://github.com/benleb/PyGlow) as well as python-smbus.

Future versions may include a makefile to add this script as a systemd service.