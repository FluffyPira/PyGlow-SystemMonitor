TARGET="$(shell pwd)/pyglow-monitor.service"
LINK_NAME="/etc/systemd/system/pyglow-monitor.service"
 
service:
	cp ${TARGET} ${LINK_NAME}
 
clean:
	rm -f ${LINK_NAME}
 
.PHONY: service
