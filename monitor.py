#####
#
# PyGlow
#
#####
#
# Python module to control Pimoronis PiGlow
# [http://shop.pimoroni.com/products/piglow]
#
# * based on cpu percentage utilisation indicator by Jason (@Boeeerb)
# [https://github.com/Boeeerb/PiGlow]
# requires psutil
# - sudo apt-get install python-psutil
# - sudo yum install python-psutil
#
# * refactored and expanded by Elizabeth (@FluffyPira)
#
#####


from PyGlow import PyGlow
from time import sleep
from random import randint
import psutil


pyglow = PyGlow()

duration = 12.0
delay = 0.1
loop_iterations = int(duration / delay)

def individual_color(percentage, threshold, color, intensity = 20):
    if percentage >= threshold:
        pyglow.color(color, intensity)
    else:
        pyglow.color(color, 0)

def individual_led(percentage, threshold, number, intensity = 20):
    if percentage >= threshold:
        pyglow.led(number, intensity)
    else:
        pyglow.led(number, 0)

def cpu_percentage():
    return psutil.cpu_percent()

def mem_percentage():
    return psutil.virtual_memory().percent

def disk_percentage():
    return psutil.disk_usage('/').percent

def cpu_temperature():
    if hasattr(psutil, "sensors_temperatures"):
        temps = psutil.sensors_temperatures()
    for name, entries in temps.items():
        for entry in entries:
            temp_percent = ((entry.current / 85) * 100)
            return temp_percent

def cpu_loop():
    return indicator_loop(cpu_percentage, 13)

def mem_loop():
    return indicator_loop(mem_percentage, 14)

def disk_loop():
    return indicator_loop(disk_percentage, 15)

def temp_loop():
    return indicator_loop(cpu_temperature, 16)

def indicator_loop(fn, indicator_led):

    for x in range(0, loop_iterations):
        value = fn()

        individual_led(value, 0,   1,  5)
        individual_led(value, 5,   2,  15)
        individual_led(value, 20,   3,  25)
        individual_led(value, 40,   4,  35)
        individual_led(value, 60,   5,  45)
        individual_led(value, 80,   6,  55)
        individual_led(value, 1,   7,  10)
        individual_led(value, 10,   8, 20)
        individual_led(value, 30,   9,  30)
        individual_led(value, 50,   10,  40)
        individual_led(value, 70,   11,  50)
        individual_led(value, 90,   12,  60)
        individual_led(value, 0,   indicator_led,  50)

        sleep(delay)

def animation_1():
    colors = ["red", "orange", "yellow", "green", "blue", "white"]

    for color in colors:
        pyglow.color(color, 25)
        sleep(0.2)

    sleep(0.6)

    for color in reversed(colors):
        pyglow.color(color, 0)
        sleep(0.2)

def animation_2():

    for led in range(1, 19):
        pyglow.led(led, 25)
        sleep (0.075)

    sleep(0.3)

    for led in reversed(range(1, 19)):
        pyglow.led(led, 0)
        sleep (0.075)

def animation_3():

    for led in range(1, 7):
        for arm in range(0, 3):
            pyglow.led(led + (6 * arm), 25)
            sleep (0.075)

    sleep(0.3)

    for led in reversed(range(1, 7)):
        for arm in reversed(range(0, 3)):
            pyglow.led(led + (6 * arm), 0)
            sleep (0.075)

# Random flickers; 1-3 per thing.
def animation_4():
    for i in range(0, 40):
        leds = []
        iterations = randint(1,3)
        for x in range(0, iterations):
            leds.append(randint(1, 18))
            for led in leds:
                pyglow.led(led, 25)
            sleep (0.075)
            for led in leds:
                pyglow.led(led, 0)

def boot_animation():
    colors = ["red", "orange", "yellow", "green", "blue", "white"]

    pyglow.all(0)

    for led in range(1, 19):
        pyglow.led(led, 25)
        sleep (0.1)
        pyglow.led(led, 50)
        sleep (0.1)
        pyglow.led(led, 0)
        sleep (0.3)
    for arm in range(1, 4):
        pyglow.arm(arm, 25)
        sleep (0.1)
        pyglow.arm(arm, 50)
        sleep (0.1)
        pyglow.arm(arm, 0)
        sleep (0.3)
    for color in colors:
        pyglow.color(color, 25)
        sleep(0.1)
        pyglow.color(color, 50)
        sleep(0.1)
        pyglow.color(color, 0)
        sleep(0.3)

    pyglow.all(25)
    sleep(0.1)
    pyglow.all(50)
    sleep(0.1)
    pyglow.all(0)
    sleep(0.3)

boot_animation()

while True:
    for fn in [animation_1, cpu_loop, animation_2, mem_loop, animation_3, disk_loop, animation_4, temp_loop]:
        pyglow.all(0)
        sleep(0.25)
        fn()
